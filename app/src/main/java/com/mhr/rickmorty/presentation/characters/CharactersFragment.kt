package com.mhr.rickmorty.presentation.characters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mhr.rickmorty.App
import com.mhr.rickmorty.R
import com.mhr.rickmorty.databinding.ViewCharactersBinding
import com.mhr.rickmorty.presentation.characters.characterdetail.ListToDetailViewModel
import com.mhr.rickmorty.presentation.model.CharacterItem
import kotlinx.android.synthetic.main.view_characters.*
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class CharactersFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val selectionViewModel: ListToDetailViewModel by activityViewModels()

    private lateinit var viewModel: CharactersViewModel
    private lateinit var adapter: CharactersAdapter
    private val returnTopButtonVisibilityThreshold = 10
    private val returnTopButtonVisibilityDuration = 2500L
    private val stateBundlePositionKey = "POSITION_KEY"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: ViewCharactersBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.view_characters,
            container,
            false
        )

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewLifecycleOwner.lifecycleScope.cancel()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(
            stateBundlePositionKey,
            getFirstVisibleItemPosition()
        )
        super.onSaveInstanceState(outState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        App.component
            .viewModelComponentFactory
            .create()
            .inject(this)

        viewModel =
            ViewModelProvider(viewModelStore, viewModelFactory)[CharactersViewModel::class.java]

        setupViewInteractions(savedInstanceState)
        setupDataInteractions(savedInstanceState)

    }

    private fun setupViewInteractions(savedInstanceState: Bundle?) {

        adapter = CharactersAdapter(object : CharactersAdapter.OnItemSelectedListener {
            override fun selected(item: CharacterItem) {
                selectionViewModel.selectedItem.value = item
                selectionViewModel.lastListPosition = getFirstVisibleItemPosition()
                findNavController().navigate(R.id.action_charactersFragment_to_characterDetailFragment)
            }
        })
        charactersRecyclerView.adapter = adapter

        charactersFetchErrorActionButton.setOnClickListener {
            viewModel.fetchData()
        }

        charactersReturnTopButton.setOnClickListener {
            charactersRecyclerView.smoothScrollToPosition(0)
        }

        charactersRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        view?.let {
                            viewLifecycleOwner.lifecycleScope.launch {
                                delay(returnTopButtonVisibilityDuration)
                                if (charactersReturnTopButton.visibility != View.GONE) {
                                    charactersReturnTopButton.visibility = View.GONE
                                }
                            }
                        }
                    }
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                (charactersRecyclerView.layoutManager as? GridLayoutManager)?.let {

                    //region Return Top visibility
                    if (it.findLastVisibleItemPosition() >= returnTopButtonVisibilityThreshold) {
                        charactersReturnTopButton.visibility = View.VISIBLE
                    } else {
                        charactersReturnTopButton.visibility = View.GONE
                    }
                    //endregion

                    //region Pagination
                    if (it.findLastCompletelyVisibleItemPosition() == it.itemCount - 1 ||
                        it.findLastCompletelyVisibleItemPosition() == it.itemCount &&
                        viewModel.viewState.value != CharactersViewState.PaginationLoading
                    ) {
                        viewModel.paginate()
                    }
                    //endregion
                }

            }

        })

    }

    private fun setupDataInteractions(savedInstanceState: Bundle?) {

        if (savedInstanceState == null && selectionViewModel.selectedItem.value == null) {
            viewModel.fetchData()
        } else {
            viewModel.restoreState()
        }

        viewModel.viewState.observe(this,
            Observer<CharactersViewState> {
                it?.let { state ->
                    handleViewState(state)
                }
            })

        //retaining last position after the data is set
        fetchLastPosition(savedInstanceState)?.let {
            viewLifecycleOwner.lifecycleScope.launch {
                charactersRecyclerView?.scrollToPosition(it)
            }
        }

    }

    private fun handleViewState(state: CharactersViewState) {
        when (state) {

            is CharactersViewState.Error -> {
                charactersMainProgressBar.hide()
                charactersPaginationProgressBar.hide()
                charactersFetchErrorGroup.visibility = View.VISIBLE
                charactersRecyclerView.visibility = View.GONE
                handleErrorMessage(state.message)
            }

            is CharactersViewState.PaginationError -> {
                charactersMainProgressBar.hide()
                charactersPaginationProgressBar.hide()
                handleErrorMessage(state.message)
            }

            is CharactersViewState.Success -> {
                charactersMainProgressBar.hide()
                charactersPaginationProgressBar.hide()
                charactersFetchErrorGroup.visibility = View.GONE
                charactersRecyclerView.visibility = View.VISIBLE
                adapter.clear()
                adapter.update(state.data)
            }

            is CharactersViewState.PaginationSuccess -> {
                charactersFetchErrorGroup.visibility = View.GONE
                charactersMainProgressBar.hide()
                charactersPaginationProgressBar.hide()
                adapter.update(state.data)
            }

            is CharactersViewState.Loading -> {
                charactersFetchErrorGroup.visibility = View.GONE
                charactersPaginationProgressBar.hide()
                charactersMainProgressBar.show()
            }

            is CharactersViewState.PaginationLoading -> {
                charactersFetchErrorGroup.visibility = View.GONE
                charactersPaginationProgressBar.show()
                charactersMainProgressBar.hide()
            }

            is CharactersViewState.RestoredData -> {
                charactersMainProgressBar.hide()
                charactersPaginationProgressBar.hide()
                charactersFetchErrorGroup.visibility = View.GONE
                charactersRecyclerView.visibility = View.VISIBLE
                adapter.clear()
                adapter.update(state.data)
            }

        }
    }

    private fun handleErrorMessage(message: String?) {
        val messageToDisplay = message ?: context?.getString(R.string.general_error)
        Toast.makeText(context, messageToDisplay, Toast.LENGTH_SHORT).show()
    }

    private fun getFirstVisibleItemPosition(): Int {
        (charactersRecyclerView?.layoutManager as? GridLayoutManager)
            ?.let {
                return it.findFirstCompletelyVisibleItemPosition()
            }
        return 0
    }

    private fun fetchLastPosition(savedInstanceState: Bundle?): Int? {
        return savedInstanceState?.getInt(stateBundlePositionKey)
            ?: selectionViewModel.lastListPosition.takeIf { it != 0 }
    }

}