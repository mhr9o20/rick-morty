package com.mhr.rickmorty.di

import com.mhr.rickmorty.presentation.characters.CharactersFragment
import dagger.BindsInstance
import dagger.Component
import dagger.Subcomponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance retrofit: Retrofit): AppComponent
    }

    val viewModelComponentFactory: ViewModelComponent.Factory

}

@Subcomponent(modules = [ViewModelModule::class])
interface ViewModelComponent {

    fun inject(charactersFragment: CharactersFragment)

    @Subcomponent.Factory
    interface Factory {
        fun create(): ViewModelComponent
    }

}