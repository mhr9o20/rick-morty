package com.mhr.rickmorty.presentation.characters

import androidx.recyclerview.widget.RecyclerView
import com.mhr.rickmorty.databinding.ViewCharacterItemBinding
import kotlinx.android.synthetic.main.view_character_item.view.*

class CharacterViewHolder(val binding: ViewCharacterItemBinding) : RecyclerView.ViewHolder(binding.root) {
    init {
        // To make marquee work
        binding.root.characterNameTextView?.isSelected = true
    }
}