package com.mhr.rickmorty.presentation.model

import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.textview.MaterialTextView
import com.mhr.rickmorty.R
import com.mhr.rickmorty.domain.model.Character
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_FEMALE
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_MALE
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_NO
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_UNKNOWN
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_ALIVE
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_DEAD
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_UNKNOWN

/*
Domain Model:
    data class Character(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val originLocationName: String,
    val lastKnownLocationName: String,
    val imageUrl: String,
    val totalAppearance: Int,
    val firstAppearance: Int
)
 */
data class CharacterItem(
    val id: Int,
    val name: String,
    val status: Status,
    val species: String,
    val type: String,
    val gender: Gender,
    val originLocationName: String,
    val lastKnownLocationName: String,
    val imageUrl: String,
    val totalAppearance: Int,
    val firstAppearance: Int
) {

    companion object {

        @DrawableRes const val placeHolder: Int = R.drawable.ph_character_avatar

        @BindingAdapter("imageUrl")
        @JvmStatic
        fun avatar(host: ImageView, imageUrl: String?) {
            imageUrl?.let {
                Glide.with(host)
                    .setDefaultRequestOptions(
                        RequestOptions().circleCrop()
                    )
                    .load(imageUrl)
                    .placeholder(placeHolder)
                    .into(host)
            } ?: run {
                host.setImageResource(placeHolder)
            }
        }

        @BindingAdapter("statusDrawableEndCompat")
        @JvmStatic
        fun statusDrawableEndCompat(host: MaterialTextView, @DrawableRes res: Int) {
            val drawable = VectorDrawableCompat.create(
                host.context.resources,
                res,
                host.context.theme
            )
            host.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null, null, drawable, null
            )
        }

        @BindingAdapter("genderDrawableEndCompat")
        @JvmStatic
        fun genderDrawableEndCompat(host: MaterialTextView, @DrawableRes res: Int) {
            val drawable = VectorDrawableCompat.create(
                host.context.resources,
                res,
                host.context.theme
            )
            host.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null, null, drawable, null
            )
        }

        @BindingAdapter("totalAppearance")
        @JvmStatic
        fun totalAppearance(host: MaterialTextView, count: Int) {
            val displayText = host.context.getString(R.string.total_appearance)
                .replace("%s", "$count")
            host.text = displayText
        }

        @BindingAdapter("originLocation")
        @JvmStatic
        fun originLocation(host: MaterialTextView, name: String) {
            val displayText = host.context.getString(R.string.origin_location)
                .replace("%s", name)
            host.text = displayText
        }

        @BindingAdapter("lastKnownLocation")
        @JvmStatic
        fun lastKnownLocation(host: MaterialTextView, name: String) {
            val displayText = host.context.getString(R.string.last_known_location)
                .replace("%s", name)
            host.text = displayText
        }

    }

}


fun CharacterItem.mapToDomainModel(): Character {

    val status = when (this.status) {
        Status.Alive -> STATUS_ALIVE
        Status.Dead -> STATUS_DEAD
        else -> STATUS_UNKNOWN
    }

    val gender = when (this.gender) {
        Gender.Female -> GENDER_FEMALE
        Gender.Male -> GENDER_MALE
        Gender.Genderless -> GENDER_NO
        else -> GENDER_UNKNOWN
    }

    return Character(
        id = id,
        name = name,
        status = status,
        species = species,
        type = type,
        gender = gender,
        originLocationName = originLocationName,
        lastKnownLocationName = lastKnownLocationName,
        imageUrl = imageUrl,
        totalAppearance = totalAppearance,
        firstAppearance = firstAppearance
    )

}

fun List<CharacterItem>.mapToDomainModel(): List<Character> = map { it.mapToDomainModel() }