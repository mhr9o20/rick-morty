package com.mhr.rickmorty.domain.model

import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_FEMALE
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_MALE
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_NO
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_ALIVE
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_DEAD
import com.mhr.rickmorty.presentation.model.CharacterItem
import com.mhr.rickmorty.presentation.model.Gender
import com.mhr.rickmorty.presentation.model.Status

/*
Entities:
    data class CharacterEntity(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("status") val status: String,
    @SerializedName("species") val species: String,
    @SerializedName("type") val type: String,
    @SerializedName("gender") val gender: String,
    @SerializedName("origin") val originLocation: LocationEntity,
    @SerializedName("location") val lastKnownLocation: LocationEntity,
    @SerializedName("image") val imageUrl: String,
    @SerializedName("episode") val episodeUrls: List<String>,
    @SerializedName("url") val ownPageUrl: String,
    @SerializedName("created") val creationDate: String
)

    data class LocationEntity(
    @SerializedName("name") val name: String,
    @SerializedName("url") val url: String
)
 */
data class Character(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val originLocationName: String,
    val lastKnownLocationName: String,
    val imageUrl: String,
    val totalAppearance: Int,
    val firstAppearance: Int
) {

    companion object {

        const val STATUS_ALIVE = "Alive"
        const val STATUS_DEAD = "Dead"
        const val STATUS_UNKNOWN = "unknown"

        const val GENDER_FEMALE = "Female"
        const val GENDER_MALE = "Male"
        const val GENDER_NO = "Genderless"
        const val GENDER_UNKNOWN = "unknown"

    }

}

fun Character.mapToPresentationModel(): CharacterItem {

    val status: Status = when(this.status) {
        STATUS_ALIVE -> Status.Alive
        STATUS_DEAD -> Status.Dead
        else -> Status.Unknown
    }

    val gender: Gender = when(this.gender) {
        GENDER_FEMALE -> Gender.Female
        GENDER_MALE -> Gender.Male
        GENDER_NO -> Gender.Genderless
        else -> Gender.Unknown
    }

    return CharacterItem(
        id = id,
        name = name,
        status = status,
        species = species,
        type = type,
        gender = gender,
        originLocationName = originLocationName,
        lastKnownLocationName = lastKnownLocationName,
        imageUrl = imageUrl,
        totalAppearance = totalAppearance,
        firstAppearance = firstAppearance
    )

}

fun List<Character>.mapToPresentaionModel(): List<CharacterItem> = map { it.mapToPresentationModel() }