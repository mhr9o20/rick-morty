package com.mhr.rickmorty.presentation.model

import com.mhr.rickmorty.domain.model.Character
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_FEMALE
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_MALE
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_NO
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_UNKNOWN
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_ALIVE
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_DEAD
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_UNKNOWN
import org.junit.Test

class CharacterItemTest {

    private val singlePresentationModel = CharacterItem(
        id = 1,
        name = "Rick",
        status = Status.Alive,
        type = "type",
        species = "species",
        gender = Gender.Male,
        originLocationName = "Earth",
        lastKnownLocationName = "Unknown",
        imageUrl = "url",
        totalAppearance = 50,
        firstAppearance = 1
    )

    private val presentationModelsList: List<CharacterItem> = listOf(
        singlePresentationModel,
        CharacterItem(
            2, "n1", Status.Dead, "s", "t", Gender.Genderless, "o", "l", "i", 1, 2
        ),
        CharacterItem(
            3, "n2", Status.Unknown, "s", "t", Gender.Female, "o", "l", "i", 3, 2
        ),
        CharacterItem(
            4, "n3", Status.Unknown, "s", "t", Gender.Unknown, "o", "l", "o", 5, 3
        )

    )

    @Test
    fun testSingleMapping() {
        val domainModel = singlePresentationModel.mapToDomainModel()
        assertPresentationToDomain(singlePresentationModel, domainModel)
    }

    @Test
    fun testListMapping() {
        val domainModels = presentationModelsList.mapToDomainModel()
        domainModels.forEachIndexed { index, character ->
            assertPresentationToDomain(
                presentation = presentationModelsList[index],
                domain = character
            )
        }
    }

    private fun assertPresentationToDomain(presentation: CharacterItem, domain: Character) {

        val status = when (presentation.status) {
            Status.Alive -> STATUS_ALIVE
            Status.Dead -> STATUS_DEAD
            else -> STATUS_UNKNOWN
        }

        val gender = when (presentation.gender) {
            Gender.Female -> GENDER_FEMALE
            Gender.Male -> GENDER_MALE
            Gender.Genderless -> GENDER_NO
            else -> GENDER_UNKNOWN
        }

        assert(domain.id == presentation.id)
        assert(domain.name == presentation.name)
        assert(domain.totalAppearance == presentation.totalAppearance)
        assert(domain.firstAppearance == presentation.firstAppearance)
        assert(domain.imageUrl == presentation.imageUrl)
        assert(domain.type == presentation.type)
        assert(domain.species == presentation.species)
        assert(domain.originLocationName == presentation.originLocationName)
        assert(domain.lastKnownLocationName == presentation.lastKnownLocationName)
        assert(domain.status == status)
        assert(domain.gender == gender)

    }

}