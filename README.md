# Rick & Morty
## Overview

***Rick & Morty*** is a simple application, showing characters of "Rick & Morty" TV series by the order of their first appearance in the show.
By clicking on each Character, more information will be presented.

## Technical Overview
The app is developed upon MVVM architecture. It has a single Data-Source which is Remote/Web-Service according to this [API](https://rickandmortyapi.com/documentation/#rest)

We're dealing with two Entities:

 - Episode
 - Character

As there's no direct API to fetch characters by our desired order, first we fetch Episodes, which are already ordered by the server, then we fetch the Characters presented in each Episode.
This way, we can be sure about the validation of our data, by lazy loading the next episodes/characters.

> Business Logic is unit-tested.

DataFlow is provided using Dagger2 and finally at the domain-layer [Use-Cases] is injected into ViewModels.

The master-detail flow is implemented using a shared ViewModel, as Android's documentation and practices suggest.

## How to Run
You can clone the project through `HTTPS` using `git clone https://mhr9o20@bitbucket.org/mhr9o20/rick-morty.git`
or via `SSH` using `git clone git@bitbucket.org:mhr9o20/rick-morty.git`




## Design
App supports Material DayNight theme, so you can enjoy the dark one on Android 10 devices.
The theme pallettes are:

 - [Day Theme](https://material.io/resources/color/#!/?view.left=0&view.right=1&primary.color=69c8ec&secondary.color=FB6467&primary.text.color=222222&secondary.text.color=222222)
 - [Night Theme](https://material.io/resources/color/#!/?view.left=0&view.right=1&primary.color=151e39&secondary.color=FB6467&secondary.text.color=222222)

The Colour Platte is inspired by [Rick and Morty Color Palettes](https://nanx.me/ggsci/reference/pal_rickandmorty.html).

The illustrations [App Icon & Error State Placeholder] are created using [Figma](https://www.figma.com/) from [this](https://icons8.com/icons/set/rick-sanchez) icon set from [Icons8](https://icons8.com).

Icons are from AndroidStudio built-in Material Icon pack.
The illustration icons are made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](http://www.flaticon.com)
