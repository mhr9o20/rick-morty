package com.mhr.rickmorty.datasource.api

import com.mhr.rickmorty.data.datasource.api.EpisodeApi
import com.mhr.rickmorty.data.datasource.remote.EpisodeRemoteDataSource
import com.mhr.rickmorty.data.model.mapToDomainModel
import com.mhr.rickmorty.domain.model.Episode

class EpisodeRemoteDataSourceImpl(private val episodeApi: EpisodeApi) : EpisodeRemoteDataSource {

    private var canPaginateEpisodes = true

    override suspend fun getEpisodes(page: Int): List<Episode> {
        val response = episodeApi.getAllEpisodes(page)
        canPaginateEpisodes = response.info.nextPageUrl.isNotBlank()
        return response.episodes.mapToDomainModel()
    }

    override val canPaginate: Boolean
        get() = canPaginateEpisodes

}

