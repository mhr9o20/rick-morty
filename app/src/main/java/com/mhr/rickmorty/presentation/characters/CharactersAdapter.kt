package com.mhr.rickmorty.presentation.characters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mhr.rickmorty.R
import com.mhr.rickmorty.presentation.model.CharacterItem

class CharactersAdapter(
    private val onItemSelectedListener: OnItemSelectedListener
) : RecyclerView.Adapter<CharacterViewHolder>() {

    private val items: MutableList<CharacterItem>

    init {
        setHasStableIds(true)
        items = mutableListOf()
    }

    override fun getItemId(position: Int): Long {
        return if (position >= 0 && position < items.size) {
            items[position].id.toLong()
        } else {
            super.getItemId(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        return CharacterViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_character_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        if (position >= 0 && position < items.size) {
            holder.binding.character = items[position]
            holder.binding.callback = this
        }
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    fun update(data: List<CharacterItem>) {
        items.addAll(data)
        notifyDataSetChanged()
    }

    fun onItemSelected(item: CharacterItem) {
        onItemSelectedListener.selected(item)
    }

    interface OnItemSelectedListener {
        fun selected(item: CharacterItem)
    }

}