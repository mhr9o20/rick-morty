package com.mhr.rickmorty.data.datasource.remote

import com.mhr.rickmorty.domain.model.Episode

interface EpisodeRemoteDataSource {

    suspend fun getEpisodes(page: Int = 1): List<Episode>

    val canPaginate: Boolean

}