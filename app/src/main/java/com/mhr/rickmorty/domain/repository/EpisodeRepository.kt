package com.mhr.rickmorty.domain.repository

import com.mhr.rickmorty.domain.model.Episode

interface EpisodeRepository {

    suspend fun getAllBy(page: Int = 1): List<Episode>
    val canPaginate: Boolean

}