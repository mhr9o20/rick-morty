package com.mhr.rickmorty.presentation.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.mhr.rickmorty.R

enum class Status(
    @DrawableRes val icon: Int,
    @StringRes val title: Int
) {
    Alive(R.drawable.ic_status_alive_24dp, R.string.status_alive),
    Dead(R.drawable.ic_status_dead_24dp, R.string.status_dead),
    Unknown(R.drawable.ic_status_unknown_24dp, R.string.status_unknown)
}