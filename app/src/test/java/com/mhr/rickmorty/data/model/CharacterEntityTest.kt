package com.mhr.rickmorty.data.model

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mhr.rickmorty.domain.model.Character
import org.junit.Test

class CharacterEntityTest {

    //region Json Object
    private val singleCharacterJson = "{\n" +
            "\"id\":1,\n" +
            "\"name\":\"Rick Sanchez\",\n" +
            "\"status\":\"Alive\",\n" +
            "\"species\":\"Human\",\n" +
            "\"type\":\"\",\n" +
            "\"gender\":\"Male\",\n" +
            "\"origin\":{\n" +
            "\"name\":\"Earth (C-137)\",\n" +
            "\"url\":\"https://rickandmortyapi.com/api/location/1\"\n" +
            "},\n" +
            "\"location\":{\n" +
            "\"name\":\"Earth (Replacement Dimension)\",\n" +
            "\"url\":\"https://rickandmortyapi.com/api/location/20\"\n" +
            "},\n" +
            "\"image\":\"https://rickandmortyapi.com/api/character/avatar/1.jpeg\",\n" +
            "\"episode\":[\n" +
            "\"https://rickandmortyapi.com/api/episode/1\",\n" +
            "\"https://rickandmortyapi.com/api/episode/2\",\n" +
            "\"https://rickandmortyapi.com/api/episode/3\",\n" +
            "\"https://rickandmortyapi.com/api/episode/4\",\n" +
            "\"https://rickandmortyapi.com/api/episode/5\",\n" +
            "\"https://rickandmortyapi.com/api/episode/6\",\n" +
            "\"https://rickandmortyapi.com/api/episode/7\",\n" +
            "\"https://rickandmortyapi.com/api/episode/8\",\n" +
            "\"https://rickandmortyapi.com/api/episode/9\",\n" +
            "\"https://rickandmortyapi.com/api/episode/10\",\n" +
            "\"https://rickandmortyapi.com/api/episode/11\",\n" +
            "\"https://rickandmortyapi.com/api/episode/12\",\n" +
            "\"https://rickandmortyapi.com/api/episode/13\",\n" +
            "\"https://rickandmortyapi.com/api/episode/14\",\n" +
            "\"https://rickandmortyapi.com/api/episode/15\",\n" +
            "\"https://rickandmortyapi.com/api/episode/16\",\n" +
            "\"https://rickandmortyapi.com/api/episode/17\",\n" +
            "\"https://rickandmortyapi.com/api/episode/18\",\n" +
            "\"https://rickandmortyapi.com/api/episode/19\",\n" +
            "\"https://rickandmortyapi.com/api/episode/20\",\n" +
            "\"https://rickandmortyapi.com/api/episode/21\",\n" +
            "\"https://rickandmortyapi.com/api/episode/22\",\n" +
            "\"https://rickandmortyapi.com/api/episode/23\",\n" +
            "\"https://rickandmortyapi.com/api/episode/24\",\n" +
            "\"https://rickandmortyapi.com/api/episode/25\",\n" +
            "\"https://rickandmortyapi.com/api/episode/26\",\n" +
            "\"https://rickandmortyapi.com/api/episode/27\",\n" +
            "\"https://rickandmortyapi.com/api/episode/28\",\n" +
            "\"https://rickandmortyapi.com/api/episode/29\",\n" +
            "\"https://rickandmortyapi.com/api/episode/30\",\n" +
            "\"https://rickandmortyapi.com/api/episode/31\"\n" +
            "],\n" +
            "\"url\":\"https://rickandmortyapi.com/api/character/1\",\n" +
            "\"created\":\"2017-11-04T18:48:46.250Z\"\n" +
            "}"
    //endregion

    //region Json Array
    val listOfCharactersJson = "[\n" +
            "{\n" +
            "\"id\":1,\n" +
            "\"name\":\"Rick Sanchez\",\n" +
            "\"status\":\"Alive\",\n" +
            "\"species\":\"Human\",\n" +
            "\"type\":\"\",\n" +
            "\"gender\":\"Male\",\n" +
            "\"origin\":{\n" +
            "\"name\":\"Earth (C-137)\",\n" +
            "\"url\":\"https://rickandmortyapi.com/api/location/1\"\n" +
            "},\n" +
            "\"location\":{\n" +
            "\"name\":\"Earth (Replacement Dimension)\",\n" +
            "\"url\":\"https://rickandmortyapi.com/api/location/20\"\n" +
            "},\n" +
            "\"image\":\"https://rickandmortyapi.com/api/character/avatar/1.jpeg\",\n" +
            "\"episode\":[\n" +
            "\"https://rickandmortyapi.com/api/episode/1\",\n" +
            "\"https://rickandmortyapi.com/api/episode/2\",\n" +
            "\"https://rickandmortyapi.com/api/episode/3\",\n" +
            "\"https://rickandmortyapi.com/api/episode/4\",\n" +
            "\"https://rickandmortyapi.com/api/episode/5\",\n" +
            "\"https://rickandmortyapi.com/api/episode/6\",\n" +
            "\"https://rickandmortyapi.com/api/episode/7\",\n" +
            "\"https://rickandmortyapi.com/api/episode/8\",\n" +
            "\"https://rickandmortyapi.com/api/episode/9\",\n" +
            "\"https://rickandmortyapi.com/api/episode/10\",\n" +
            "\"https://rickandmortyapi.com/api/episode/11\",\n" +
            "\"https://rickandmortyapi.com/api/episode/12\",\n" +
            "\"https://rickandmortyapi.com/api/episode/13\",\n" +
            "\"https://rickandmortyapi.com/api/episode/14\",\n" +
            "\"https://rickandmortyapi.com/api/episode/15\",\n" +
            "\"https://rickandmortyapi.com/api/episode/16\",\n" +
            "\"https://rickandmortyapi.com/api/episode/17\",\n" +
            "\"https://rickandmortyapi.com/api/episode/18\",\n" +
            "\"https://rickandmortyapi.com/api/episode/19\",\n" +
            "\"https://rickandmortyapi.com/api/episode/20\",\n" +
            "\"https://rickandmortyapi.com/api/episode/21\",\n" +
            "\"https://rickandmortyapi.com/api/episode/22\",\n" +
            "\"https://rickandmortyapi.com/api/episode/23\",\n" +
            "\"https://rickandmortyapi.com/api/episode/24\",\n" +
            "\"https://rickandmortyapi.com/api/episode/25\",\n" +
            "\"https://rickandmortyapi.com/api/episode/26\",\n" +
            "\"https://rickandmortyapi.com/api/episode/27\",\n" +
            "\"https://rickandmortyapi.com/api/episode/28\",\n" +
            "\"https://rickandmortyapi.com/api/episode/29\",\n" +
            "\"https://rickandmortyapi.com/api/episode/30\",\n" +
            "\"https://rickandmortyapi.com/api/episode/31\"\n" +
            "],\n" +
            "\"url\":\"https://rickandmortyapi.com/api/character/1\",\n" +
            "\"created\":\"2017-11-04T18:48:46.250Z\"\n" +
            "},\n" +
            "{\n" +
            "\"id\":2,\n" +
            "\"name\":\"Morty Smith\",\n" +
            "\"status\":\"Alive\",\n" +
            "\"species\":\"Human\",\n" +
            "\"type\":\"\",\n" +
            "\"gender\":\"Male\",\n" +
            "\"origin\":{\n" +
            "\"name\":\"Earth (C-137)\",\n" +
            "\"url\":\"https://rickandmortyapi.com/api/location/1\"\n" +
            "},\n" +
            "\"location\":{\n" +
            "\"name\":\"Earth (Replacement Dimension)\",\n" +
            "\"url\":\"https://rickandmortyapi.com/api/location/20\"\n" +
            "},\n" +
            "\"image\":\"https://rickandmortyapi.com/api/character/avatar/2.jpeg\",\n" +
            "\"episode\":[\n" +
            "\"https://rickandmortyapi.com/api/episode/1\",\n" +
            "\"https://rickandmortyapi.com/api/episode/2\",\n" +
            "\"https://rickandmortyapi.com/api/episode/3\",\n" +
            "\"https://rickandmortyapi.com/api/episode/4\",\n" +
            "\"https://rickandmortyapi.com/api/episode/5\",\n" +
            "\"https://rickandmortyapi.com/api/episode/6\",\n" +
            "\"https://rickandmortyapi.com/api/episode/7\",\n" +
            "\"https://rickandmortyapi.com/api/episode/8\",\n" +
            "\"https://rickandmortyapi.com/api/episode/9\",\n" +
            "\"https://rickandmortyapi.com/api/episode/10\",\n" +
            "\"https://rickandmortyapi.com/api/episode/11\",\n" +
            "\"https://rickandmortyapi.com/api/episode/12\",\n" +
            "\"https://rickandmortyapi.com/api/episode/13\",\n" +
            "\"https://rickandmortyapi.com/api/episode/14\",\n" +
            "\"https://rickandmortyapi.com/api/episode/15\",\n" +
            "\"https://rickandmortyapi.com/api/episode/16\",\n" +
            "\"https://rickandmortyapi.com/api/episode/17\",\n" +
            "\"https://rickandmortyapi.com/api/episode/18\",\n" +
            "\"https://rickandmortyapi.com/api/episode/19\",\n" +
            "\"https://rickandmortyapi.com/api/episode/20\",\n" +
            "\"https://rickandmortyapi.com/api/episode/21\",\n" +
            "\"https://rickandmortyapi.com/api/episode/22\",\n" +
            "\"https://rickandmortyapi.com/api/episode/23\",\n" +
            "\"https://rickandmortyapi.com/api/episode/24\",\n" +
            "\"https://rickandmortyapi.com/api/episode/25\",\n" +
            "\"https://rickandmortyapi.com/api/episode/26\",\n" +
            "\"https://rickandmortyapi.com/api/episode/27\",\n" +
            "\"https://rickandmortyapi.com/api/episode/28\",\n" +
            "\"https://rickandmortyapi.com/api/episode/29\",\n" +
            "\"https://rickandmortyapi.com/api/episode/30\",\n" +
            "\"https://rickandmortyapi.com/api/episode/31\"\n" +
            "],\n" +
            "\"url\":\"https://rickandmortyapi.com/api/character/2\",\n" +
            "\"created\":\"2017-11-04T18:50:21.651Z\"\n" +
            "},\n" +
            "{\n" +
            "\"id\":3,\n" +
            "\"name\":\"Summer Smith\",\n" +
            "\"status\":\"Alive\",\n" +
            "\"species\":\"Human\",\n" +
            "\"type\":\"\",\n" +
            "\"gender\":\"Female\",\n" +
            "\"origin\":{\n" +
            "\"name\":\"Earth (Replacement Dimension)\",\n" +
            "\"url\":\"https://rickandmortyapi.com/api/location/20\"\n" +
            "},\n" +
            "\"location\":{\n" +
            "\"name\":\"Earth (Replacement Dimension)\",\n" +
            "\"url\":\"https://rickandmortyapi.com/api/location/20\"\n" +
            "},\n" +
            "\"image\":\"https://rickandmortyapi.com/api/character/avatar/3.jpeg\",\n" +
            "\"episode\":[\n" +
            "\"https://rickandmortyapi.com/api/episode/6\",\n" +
            "\"https://rickandmortyapi.com/api/episode/7\",\n" +
            "\"https://rickandmortyapi.com/api/episode/8\",\n" +
            "\"https://rickandmortyapi.com/api/episode/9\",\n" +
            "\"https://rickandmortyapi.com/api/episode/10\",\n" +
            "\"https://rickandmortyapi.com/api/episode/11\",\n" +
            "\"https://rickandmortyapi.com/api/episode/12\",\n" +
            "\"https://rickandmortyapi.com/api/episode/14\",\n" +
            "\"https://rickandmortyapi.com/api/episode/15\",\n" +
            "\"https://rickandmortyapi.com/api/episode/16\",\n" +
            "\"https://rickandmortyapi.com/api/episode/17\",\n" +
            "\"https://rickandmortyapi.com/api/episode/18\",\n" +
            "\"https://rickandmortyapi.com/api/episode/19\",\n" +
            "\"https://rickandmortyapi.com/api/episode/20\",\n" +
            "\"https://rickandmortyapi.com/api/episode/21\",\n" +
            "\"https://rickandmortyapi.com/api/episode/22\",\n" +
            "\"https://rickandmortyapi.com/api/episode/23\",\n" +
            "\"https://rickandmortyapi.com/api/episode/24\",\n" +
            "\"https://rickandmortyapi.com/api/episode/25\",\n" +
            "\"https://rickandmortyapi.com/api/episode/26\",\n" +
            "\"https://rickandmortyapi.com/api/episode/27\",\n" +
            "\"https://rickandmortyapi.com/api/episode/29\",\n" +
            "\"https://rickandmortyapi.com/api/episode/30\",\n" +
            "\"https://rickandmortyapi.com/api/episode/31\"\n" +
            "],\n" +
            "\"url\":\"https://rickandmortyapi.com/api/character/3\",\n" +
            "\"created\":\"2017-11-04T19:09:56.428Z\"\n" +
            "}]"
    //endregion

    private val gson = Gson()

    @Test
    fun testSingleMappingToDomain() {
        val characterEntity = gson.fromJson<CharacterEntity>(
            singleCharacterJson,
            CharacterEntity::class.java
        )
        val character = characterEntity.mapToDomainModel()

        assertEntityToDomain(characterEntity, character)
        //according to our sample data inorder not to bias the logic
        assert(character.firstAppearance == 1)

    }

    @Test
    fun testListMappingToDomain() {
        val type = object : TypeToken<List<CharacterEntity>>() {}.type
        val characterEntities = gson.fromJson<List<CharacterEntity>>(listOfCharactersJson, type)
        val characters = characterEntities.mapToDomainModel()

        assert(characters.size == characterEntities.size)

        characters.forEachIndexed { index, character ->
            assertEntityToDomain(
                characterEntities[index],
                character
            )
            //according to our sample data inorder not to bias the logic
            when (index) {
                0 -> assert(character.firstAppearance == 1)
                1 -> assert(character.firstAppearance == 1)
                2 -> assert(character.firstAppearance == 6)
            }
        }

    }

    private fun assertEntityToDomain(entity: CharacterEntity, character: Character) {

        assert(character.id == entity.id)
        assert(character.name == entity.name)
        assert(character.gender == entity.gender)
        assert(character.species == entity.species)
        assert(character.type == entity.type)
        assert(character.status == entity.status)
        assert(character.imageUrl == entity.imageUrl)
        assert(character.originLocationName == entity.originLocation.name)
        assert(character.lastKnownLocationName == entity.lastKnownLocation.name)
        assert(character.totalAppearance == entity.episodeUrls.size)

    }

}