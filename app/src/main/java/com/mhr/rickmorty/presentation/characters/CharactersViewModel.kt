package com.mhr.rickmorty.presentation.characters

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mhr.rickmorty.di.CoroutineContextProvider
import com.mhr.rickmorty.domain.model.Character
import com.mhr.rickmorty.domain.model.mapToPresentaionModel
import com.mhr.rickmorty.domain.usecase.CharactersByAppearanceUseCase
import com.mhr.rickmorty.presentation.model.CharacterItem
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CharactersViewModel
@Inject constructor(
    private val useCase: CharactersByAppearanceUseCase,
    private val contextProvider: CoroutineContextProvider
) : ViewModel() {

    private val viewStateLiveData = MutableLiveData<CharactersViewState>()

    private val remoteData: LinkedHashSet<Character> = linkedSetOf()
    private var currentPage = 1
    private var dataChunkSize = 20

    val viewState: LiveData<CharactersViewState>
        get() {
            return viewStateLiveData
        }

    init {
        useCase.chunkSize = dataChunkSize
    }

    private val errorHandler = CoroutineExceptionHandler { _, exception ->
        if (currentPage > 1) {
            currentPage--
            viewStateLiveData.value = CharactersViewState.PaginationError(exception.message)
        } else {
            viewStateLiveData.value = CharactersViewState.Error(exception.message)
        }
    }

    fun fetchData() {
        viewStateLiveData.value = CharactersViewState.Loading
        fetchCharacters()
    }

    fun paginate() {
        if (useCase.canPaginate) {
            viewStateLiveData.value = CharactersViewState.PaginationLoading
            currentPage++
            fetchCharacters()
        }
    }

    fun restoreState() {
        if (remoteData.isNotEmpty()) {
            viewStateLiveData.value = CharactersViewState.RestoredData(
                remoteData.toList().mapToPresentaionModel()
            )
        }
    }

    private fun fetchCharacters() {
        viewModelScope.launch(errorHandler) {
            val data = withContext(contextProvider.IO) {
                useCase.getCharactersOrderedByAppearanceBy(currentPage)
            }
            handleData(data.sortedBy { it.firstAppearance })
        }
    }

    private fun handleData(data: List<Character>) {
        // First we preserve all the remote data
        remoteData.addAll(data)

        if (remoteData.size == 0) {
            notifyPresentationDataUpdates(emptyList())
        } else {
            val chunked = remoteData.chunked(dataChunkSize)

            if (currentPage - 1 < chunked.size) {
                notifyPresentationDataUpdates(chunked[currentPage - 1].mapToPresentaionModel())
            }
        }

    }

    private fun notifyPresentationDataUpdates(presentationData: List<CharacterItem>) {
        if (currentPage == 1) {
            viewStateLiveData.value = CharactersViewState.Success(presentationData)
        } else {
            viewStateLiveData.value = CharactersViewState.PaginationSuccess(presentationData)
        }
    }

}

sealed class CharactersViewState {
    object Loading : CharactersViewState()
    object PaginationLoading : CharactersViewState()
    data class Error(val message: String?) : CharactersViewState()
    data class Success(val data: List<CharacterItem>) : CharactersViewState()
    data class PaginationSuccess(val data: List<CharacterItem>) : CharactersViewState()
    data class PaginationError(val message: String?) : CharactersViewState()
    data class RestoredData(val data: List<CharacterItem>) : CharactersViewState()
}