package com.mhr.rickmorty.presentation.characters

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.mhr.rickmorty.domain.model.Character
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_NO
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_ALIVE
import com.mhr.rickmorty.domain.model.mapToPresentaionModel
import com.mhr.rickmorty.domain.usecase.CharactersByAppearanceUseCase
import com.mhr.rickmorty.presentation.CoroutineTestRule
import com.mhr.rickmorty.presentation.TestContextProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class CharactersViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    private lateinit var viewModel: CharactersViewModel

    @Mock
    private lateinit var useCase: CharactersByAppearanceUseCase
    @Mock
    private lateinit var viewStateObserver: Observer<CharactersViewState>

    private val sampleCharacter = Character(
        1, "Name", STATUS_ALIVE, "s", "t", GENDER_NO, "o", "l", "i", 5, 1
    )

    private val domainData: MutableList<Character> = mutableListOf()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        for (i in 0 until 50) {
            domainData.add(
                Character(
                    id = i + 1,
                    name = sampleCharacter.name,
                    status = sampleCharacter.status,
                    species = sampleCharacter.species,
                    type = sampleCharacter.type,
                    gender = sampleCharacter.gender,
                    originLocationName = sampleCharacter.originLocationName,
                    lastKnownLocationName = sampleCharacter.originLocationName,
                    imageUrl = sampleCharacter.imageUrl,
                    totalAppearance = sampleCharacter.totalAppearance,
                    firstAppearance = sampleCharacter.firstAppearance
                )
            )
        }

        viewModel = CharactersViewModel(
            useCase,
            TestContextProvider()
        )

        viewModel.viewState.observeForever(viewStateObserver)

    }

    @Test
    fun `remote data fits the chunk`() {

        `when`(useCase.canPaginate).thenReturn(true)

        coroutineTestRule.runBlocking {

            `when`(useCase.getCharactersOrderedByAppearanceBy()).thenReturn(
                domainData.subList(
                    0,
                    20
                )
            )

            viewModel.fetchData()

            verify(viewStateObserver).onChanged(CharactersViewState.Loading)
            verify(viewStateObserver).onChanged(
                CharactersViewState.Success(
                    domainData.subList(
                        0,
                        20
                    ).mapToPresentaionModel()
                )
            )
            verify(viewStateObserver, never()).onChanged(CharactersViewState.PaginationLoading)
            verify(viewStateObserver, never()).onChanged(CharactersViewState.Error(anyString()))
            verify(viewStateObserver, never()).onChanged(
                CharactersViewState.PaginationError(
                    anyString()
                )
            )
            verify(viewStateObserver, never()).onChanged(
                CharactersViewState.PaginationSuccess(
                    anyList()
                )
            )
            verify(
                viewStateObserver,
                never()
            ).onChanged(CharactersViewState.RestoredData(anyList()))

        }

    }

    @Test
    fun `error on fetching data`() = coroutineTestRule.runBlocking {

        val error = Error("Error")
        `when`(useCase.canPaginate).thenReturn(true)

        `when`(useCase.getCharactersOrderedByAppearanceBy()).thenThrow(error)

        viewModel.fetchData()

        verify(viewStateObserver).onChanged(CharactersViewState.Loading)
        verify(viewStateObserver).onChanged(CharactersViewState.Error(error.message))
        verify(viewStateObserver, never()).onChanged(CharactersViewState.PaginationLoading)
        verify(
            viewStateObserver,
            never()
        ).onChanged(CharactersViewState.Success(ArgumentMatchers.anyList()))
        verify(
            viewStateObserver,
            never()
        ).onChanged(CharactersViewState.PaginationError(anyString()))
        verify(
            viewStateObserver,
            never()
        ).onChanged(CharactersViewState.PaginationSuccess(anyList()))
        verify(viewStateObserver, never()).onChanged(CharactersViewState.RestoredData(anyList()))

    }

    @Test
    fun `remote data is less than chunk`() {

        val expectedViewState =
            CharactersViewState.Success(domainData.subList(0, 10).mapToPresentaionModel())
        `when`(useCase.canPaginate).thenReturn(true)

        coroutineTestRule.runBlocking {

            `when`(useCase.getCharactersOrderedByAppearanceBy(anyInt()))
                .thenReturn(domainData.subList(0, 10))

            viewModel.fetchData()

            verify(useCase).getCharactersOrderedByAppearanceBy(1)

            verify(viewStateObserver).onChanged(CharactersViewState.Loading)
            verify(viewStateObserver).onChanged(expectedViewState)
            verify(viewStateObserver, never()).onChanged(CharactersViewState.PaginationLoading)
            verify(viewStateObserver, never()).onChanged(CharactersViewState.Error(anyString()))
            verify(viewStateObserver, never()).onChanged(
                CharactersViewState.PaginationError(
                    anyString()
                )
            )
            verify(viewStateObserver, never()).onChanged(
                CharactersViewState.PaginationSuccess(
                    anyList()
                )
            )
            verify(
                viewStateObserver,
                never()
            ).onChanged(CharactersViewState.RestoredData(anyList()))

            assert(viewModel.viewState.value == expectedViewState)
        }

    }

    @Test
    fun `pagination when remote data fits the chunk`() {

        `when`(useCase.canPaginate).thenReturn(true)

        coroutineTestRule.runBlocking {
            `when`(useCase.getCharactersOrderedByAppearanceBy(anyInt()))
                .thenReturn(domainData.subList(0, 20))
                .thenReturn(domainData.subList(20, 40))

            viewModel.fetchData()

            viewModel.paginate()

            verify(useCase, atLeast(2)).getCharactersOrderedByAppearanceBy(anyInt())
            verify(useCase, atMost(2)).getCharactersOrderedByAppearanceBy(anyInt())


            verify(viewStateObserver).onChanged(CharactersViewState.Loading)
            verify(viewStateObserver).onChanged(
                CharactersViewState.Success(
                    domainData.subList(0, 20).mapToPresentaionModel()
                )
            )
            verify(viewStateObserver).onChanged(CharactersViewState.PaginationLoading)
            verify(viewStateObserver).onChanged(
                CharactersViewState.PaginationSuccess(
                    domainData.subList(20, 40).mapToPresentaionModel()
                )
            )
            verify(viewStateObserver, never()).onChanged(CharactersViewState.Error(anyString()))
            verify(viewStateObserver, never()).onChanged(
                CharactersViewState.PaginationError(
                    anyString()
                )
            )
            verify(
                viewStateObserver,
                never()
            ).onChanged(CharactersViewState.RestoredData(anyList()))

        }

    }

    @Test
    fun `pagination when remote data doesn't fit the chunk`() {

        `when`(useCase.canPaginate).thenReturn(true)

        coroutineTestRule.runBlocking {
            `when`(useCase.getCharactersOrderedByAppearanceBy(anyInt()))
                .thenReturn(domainData.subList(0, 20))
                .thenReturn(domainData.subList(20, 40))
                .thenReturn(domainData.subList(40, 45))

            viewModel.fetchData()

            verify(useCase).getCharactersOrderedByAppearanceBy(1)

            viewModel.paginate()

            verify(useCase).getCharactersOrderedByAppearanceBy(2)

            verify(viewStateObserver).onChanged(CharactersViewState.Loading)

            verify(viewStateObserver).onChanged(CharactersViewState.PaginationLoading)

            verify(viewStateObserver).onChanged(
                CharactersViewState.Success(
                    domainData.subList(0, 20).mapToPresentaionModel()
                )
            )
            verify(viewStateObserver).onChanged(
                CharactersViewState.PaginationSuccess(
                    domainData.subList(20, 40).mapToPresentaionModel()
                )
            )

            verify(viewStateObserver, never()).onChanged(CharactersViewState.Error(anyString()))
            verify(viewStateObserver, never()).onChanged(
                CharactersViewState.PaginationError(
                    anyString()
                )
            )
            verify(
                viewStateObserver,
                never()
            ).onChanged(CharactersViewState.RestoredData(anyList()))

        }

    }

    @Test
    fun `pagination when remote data doesn't fit the chunk and we reach the end`() {

        `when`(useCase.canPaginate)
            .thenReturn(true, true, false)

        coroutineTestRule.runBlocking {
            `when`(useCase.getCharactersOrderedByAppearanceBy(anyInt()))
                .thenReturn(domainData.subList(0, 20))
                .thenReturn(domainData.subList(20, 40))
                .thenReturn(domainData.drop(40))

            viewModel.fetchData()

            verify(useCase).getCharactersOrderedByAppearanceBy(1)

            viewModel.paginate()

            verify(useCase).getCharactersOrderedByAppearanceBy(2)

            viewModel.paginate()

            verify(useCase).getCharactersOrderedByAppearanceBy(3)

            verify(viewStateObserver).onChanged(CharactersViewState.Loading)
            verify(viewStateObserver, atLeast(2)).onChanged(CharactersViewState.PaginationLoading)
            verify(viewStateObserver, atMost(2)).onChanged(CharactersViewState.PaginationLoading)

            verify(viewStateObserver).onChanged(
                CharactersViewState.Success(
                    domainData.subList(0, 20).mapToPresentaionModel()
                )
            )
            verify(viewStateObserver).onChanged(
                CharactersViewState.PaginationSuccess(
                    domainData.subList(20, 40).mapToPresentaionModel()
                )
            )
            verify(viewStateObserver).onChanged(
                CharactersViewState.PaginationSuccess(
                    domainData.drop(40).mapToPresentaionModel()
                )
            )

            verify(viewStateObserver, never()).onChanged(CharactersViewState.Error(anyString()))
            verify(viewStateObserver, never()).onChanged(
                CharactersViewState.PaginationError(
                    anyString()
                )
            )
            verify(
                viewStateObserver,
                never()
            ).onChanged(CharactersViewState.RestoredData(anyList()))

        }

    }

    @Test
    fun `verify data uniqueness`() {

        `when`(useCase.canPaginate).thenReturn(false)

        val data: MutableList<Character> = mutableListOf()

        for (i in 0 until 20) {
            val id = when {
                i % 2 == 0 -> 1
                i % 3 == 0 -> 2
                else -> 3
            }

            data.add(
                Character(
                    id = id,
                    name = sampleCharacter.name,
                    status = sampleCharacter.status,
                    species = sampleCharacter.species,
                    type = sampleCharacter.type,
                    gender = sampleCharacter.gender,
                    originLocationName = sampleCharacter.originLocationName,
                    lastKnownLocationName = sampleCharacter.originLocationName,
                    imageUrl = sampleCharacter.imageUrl,
                    totalAppearance = sampleCharacter.totalAppearance,
                    firstAppearance = sampleCharacter.firstAppearance
                )
            )
        }

        val expectedData: LinkedHashSet<Character> = linkedSetOf()
        expectedData.addAll(data)
        expectedData.sortedBy { it.firstAppearance }

        coroutineTestRule.runBlocking {

            `when`(useCase.getCharactersOrderedByAppearanceBy()).thenReturn(data)

            viewModel.fetchData()

            verify(useCase, atMost(1)).getCharactersOrderedByAppearanceBy(anyInt())
            verify(viewStateObserver).onChanged(CharactersViewState.Loading)
            verify(viewStateObserver).onChanged(
                CharactersViewState.Success(
                    expectedData.toList().mapToPresentaionModel()
                )
            )

        }

    }

    @Test
    fun `verify retry on pagination failure`() {

        `when`(useCase.canPaginate).thenReturn(true)

        val error = Error("error")

        coroutineTestRule.runBlocking {

            `when`(useCase.getCharactersOrderedByAppearanceBy(anyInt()))
                .thenReturn(domainData.subList(0, 20))
                .thenThrow(error)
                .thenReturn(domainData.subList(20, 40))

            viewModel.fetchData()
            viewModel.paginate()
            viewModel.paginate()

            verify(useCase).getCharactersOrderedByAppearanceBy(1)
            verify(useCase, atLeast(2)).getCharactersOrderedByAppearanceBy(2)
            verify(useCase, atMost(2)).getCharactersOrderedByAppearanceBy(2)

            verify(viewStateObserver).onChanged(CharactersViewState.Loading)
            verify(viewStateObserver).onChanged(
                CharactersViewState.Success(
                    domainData.subList(0, 20).mapToPresentaionModel()
                )
            )
            verify(viewStateObserver).onChanged(CharactersViewState.PaginationError(error.message))
            verify(viewStateObserver, atLeast(2)).onChanged(CharactersViewState.PaginationLoading)
            verify(viewStateObserver, atMost(2)).onChanged(CharactersViewState.PaginationLoading)
            verify(viewStateObserver).onChanged(
                CharactersViewState.PaginationSuccess(
                    domainData.subList(20, 40).mapToPresentaionModel()
                )
            )

        }

    }

    @Test
    fun `verify restoring state when list is empty`() {

        `when`(useCase.canPaginate).thenReturn(false)

        coroutineTestRule.runBlocking {

            `when`(useCase.getCharactersOrderedByAppearanceBy()).thenReturn(emptyList())

            viewModel.fetchData()
            verify(useCase, atMost(1)).getCharactersOrderedByAppearanceBy(anyInt())
            verify(viewStateObserver).onChanged(CharactersViewState.Loading)
            verify(viewStateObserver).onChanged(CharactersViewState.Success(emptyList()))

            viewModel.restoreState()

            verify(
                viewStateObserver,
                never()
            ).onChanged(CharactersViewState.RestoredData(anyList()))

        }

    }

    @Test
    fun `verify restoring state`() {

        `when`(useCase.canPaginate)
            .thenReturn(true, true, false)

        coroutineTestRule.runBlocking {
            `when`(useCase.getCharactersOrderedByAppearanceBy(anyInt()))
                .thenReturn(domainData.subList(0, 20))
                .thenReturn(domainData.subList(20, 40))
                .thenReturn(domainData.drop(40))

            viewModel.fetchData()

            verify(useCase).getCharactersOrderedByAppearanceBy(1)

            viewModel.paginate()

            verify(useCase).getCharactersOrderedByAppearanceBy(2)

            viewModel.paginate()

            verify(useCase).getCharactersOrderedByAppearanceBy(3)

            verify(viewStateObserver).onChanged(CharactersViewState.Loading)
            verify(viewStateObserver, atLeast(2)).onChanged(CharactersViewState.PaginationLoading)
            verify(viewStateObserver, atMost(2)).onChanged(CharactersViewState.PaginationLoading)

            verify(viewStateObserver).onChanged(
                CharactersViewState.Success(
                    domainData.subList(0, 20).mapToPresentaionModel()
                )
            )
            verify(viewStateObserver).onChanged(
                CharactersViewState.PaginationSuccess(
                    domainData.subList(20, 40).mapToPresentaionModel()
                )
            )
            verify(viewStateObserver).onChanged(
                CharactersViewState.PaginationSuccess(
                    domainData.drop(40).mapToPresentaionModel()
                )
            )

            verify(viewStateObserver, never()).onChanged(CharactersViewState.Error(anyString()))
            verify(viewStateObserver, never()).onChanged(
                CharactersViewState.PaginationError(
                    anyString()
                )
            )

            viewModel.restoreState()

            verify(viewStateObserver).onChanged(
                CharactersViewState.RestoredData(
                    domainData.mapToPresentaionModel()
                )
            )

        }

    }

    @After
    fun tearDown() {

    }


}