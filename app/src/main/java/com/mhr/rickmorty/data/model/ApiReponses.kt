package com.mhr.rickmorty.data.model

import com.google.gson.annotations.SerializedName

/*
"info":{
"count":493,
"pages":25,
"next":"https://rickandmortyapi.com/api/character/?page=2",
"prev":""
},
"results": [] //NOTE: chunk size is 20
 */
data class InfoPart(
    @SerializedName("count") val totalCount: Int,
    @SerializedName("pages") val totalPages: Int,
    @SerializedName("next") val nextPageUrl: String,
    @SerializedName("prev") val previousPageUrl: String
)

data class CharactersResponse(
    @SerializedName("info") val info: InfoPart,
    @SerializedName("results") val characters: List<CharacterEntity>
)

data class EpisodesReponse(
    @SerializedName("info") val info: InfoPart,
    @SerializedName("results") val episodes: List<EpisodeEntity>
)
