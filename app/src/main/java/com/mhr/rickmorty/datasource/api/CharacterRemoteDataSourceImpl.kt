package com.mhr.rickmorty.datasource.api

import com.mhr.rickmorty.data.datasource.api.CharacterApi
import com.mhr.rickmorty.data.datasource.remote.CharacterRemoteDataSource
import com.mhr.rickmorty.data.model.mapToDomainModel
import com.mhr.rickmorty.domain.model.Character

class CharacterRemoteDataSourceImpl(private val characterApi: CharacterApi) :
    CharacterRemoteDataSource {

    private var canPaginateCharacters: Boolean = true

    override suspend fun getCharacters(page: Int): List<Character> {
        val response = characterApi.getAllCharacters(page)
        canPaginateCharacters = response.info.nextPageUrl.isNotBlank()
        return response.characters.mapToDomainModel()
    }

    override val canPaginate: Boolean
        get() = canPaginateCharacters

    override suspend fun getCharactersByIds(ids: IntArray): List<Character> {

        return if (ids.size == 1) {
            listOf(characterApi.getCharacterById(ids[0]).mapToDomainModel())
        } else {
            var stringIds = ""
            ids.forEachIndexed { index, i ->
                stringIds += "$i"
                if (index != ids.size - 1) stringIds += ","
            }
            characterApi.getCharactersByIds(stringIds).mapToDomainModel()
        }

    }
}