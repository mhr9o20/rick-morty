package com.mhr.rickmorty.data.repository

import com.mhr.rickmorty.data.datasource.remote.CharacterRemoteDataSource
import com.mhr.rickmorty.domain.model.Character
import com.mhr.rickmorty.domain.repository.CharacterRepository

class CharacterRepositoryImpl(
    private val remoteDataSource: CharacterRemoteDataSource
) : CharacterRepository {

    override suspend fun getAllBy(page: Int): List<Character> {
        return remoteDataSource.getCharacters(page)
    }

    override suspend fun getBy(ids: IntArray): List<Character> {
        return remoteDataSource.getCharactersByIds(ids)
    }

    override val canPaginate: Boolean
        get() = remoteDataSource.canPaginate

}