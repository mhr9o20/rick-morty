package com.mhr.rickmorty.data.datasource.api

import com.mhr.rickmorty.data.model.CharacterEntity
import com.mhr.rickmorty.data.model.CharactersResponse
import com.mhr.rickmorty.data.model.EpisodesReponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface EpisodeApi {

    @GET(EndPoints.episode)
    suspend fun getAllEpisodes(@Query("page") page: Int): EpisodesReponse

}

interface CharacterApi {

    @GET(EndPoints.character)
    suspend fun getAllCharacters(@Query("page") page: Int): CharactersResponse

    @GET(EndPoints.character + "{ids}")
    suspend fun getCharactersByIds(@Path("ids") ids: String): List<CharacterEntity>

    @GET(EndPoints.character + "{id}")
    suspend fun getCharacterById(@Path("id") id: Int): CharacterEntity

}