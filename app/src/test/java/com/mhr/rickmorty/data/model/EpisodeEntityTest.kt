package com.mhr.rickmorty.data.model

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mhr.rickmorty.domain.model.Episode
import org.junit.Test

class EpisodeEntityTest {

    //region Json Object
    private val singleEpisodeJson = "{\n" +
            "\"id\":1,\n" +
            "\"name\":\"Pilot\",\n" +
            "\"air_date\":\"December 2, 2013\",\n" +
            "\"episode\":\"S01E01\",\n" +
            "\"characters\":[\n" +
            "\"https://rickandmortyapi.com/api/character/1\",\n" +
            "\"https://rickandmortyapi.com/api/character/2\",\n" +
            "\"https://rickandmortyapi.com/api/character/35\",\n" +
            "\"https://rickandmortyapi.com/api/character/38\",\n" +
            "\"https://rickandmortyapi.com/api/character/62\",\n" +
            "\"https://rickandmortyapi.com/api/character/92\",\n" +
            "\"https://rickandmortyapi.com/api/character/127\",\n" +
            "\"https://rickandmortyapi.com/api/character/144\",\n" +
            "\"https://rickandmortyapi.com/api/character/158\",\n" +
            "\"https://rickandmortyapi.com/api/character/175\",\n" +
            "\"https://rickandmortyapi.com/api/character/179\",\n" +
            "\"https://rickandmortyapi.com/api/character/181\",\n" +
            "\"https://rickandmortyapi.com/api/character/239\",\n" +
            "\"https://rickandmortyapi.com/api/character/249\",\n" +
            "\"https://rickandmortyapi.com/api/character/271\",\n" +
            "\"https://rickandmortyapi.com/api/character/338\",\n" +
            "\"https://rickandmortyapi.com/api/character/394\",\n" +
            "\"https://rickandmortyapi.com/api/character/395\",\n" +
            "\"https://rickandmortyapi.com/api/character/435\"\n" +
            "],\n" +
            "\"url\":\"https://rickandmortyapi.com/api/episode/1\",\n" +
            "\"created\":\"2017-11-10T12:56:33.798Z\"\n" +
            "}"
    //endregion

    //region Json Array
    private val listOfEpisodesJson = "[{\n" +
            "\"id\":1,\n" +
            "\"name\":\"Pilot\",\n" +
            "\"air_date\":\"December 2, 2013\",\n" +
            "\"episode\":\"S01E01\",\n" +
            "\"characters\":[\n" +
            "\"https://rickandmortyapi.com/api/character/1\",\n" +
            "\"https://rickandmortyapi.com/api/character/2\",\n" +
            "\"https://rickandmortyapi.com/api/character/35\",\n" +
            "\"https://rickandmortyapi.com/api/character/38\",\n" +
            "\"https://rickandmortyapi.com/api/character/62\",\n" +
            "\"https://rickandmortyapi.com/api/character/92\",\n" +
            "\"https://rickandmortyapi.com/api/character/127\",\n" +
            "\"https://rickandmortyapi.com/api/character/144\",\n" +
            "\"https://rickandmortyapi.com/api/character/158\",\n" +
            "\"https://rickandmortyapi.com/api/character/175\",\n" +
            "\"https://rickandmortyapi.com/api/character/179\",\n" +
            "\"https://rickandmortyapi.com/api/character/181\",\n" +
            "\"https://rickandmortyapi.com/api/character/239\",\n" +
            "\"https://rickandmortyapi.com/api/character/249\",\n" +
            "\"https://rickandmortyapi.com/api/character/271\",\n" +
            "\"https://rickandmortyapi.com/api/character/338\",\n" +
            "\"https://rickandmortyapi.com/api/character/394\",\n" +
            "\"https://rickandmortyapi.com/api/character/395\",\n" +
            "\"https://rickandmortyapi.com/api/character/435\"\n" +
            "],\n" +
            "\"url\":\"https://rickandmortyapi.com/api/episode/1\",\n" +
            "\"created\":\"2017-11-10T12:56:33.798Z\"\n" +
            "},\n" +
            "{\n" +
            "\"id\":2,\n" +
            "\"name\":\"Lawnmower Dog\",\n" +
            "\"air_date\":\"December 9, 2013\",\n" +
            "\"episode\":\"S01E02\",\n" +
            "\"characters\":[\n" +
            "\"https://rickandmortyapi.com/api/character/1\",\n" +
            "\"https://rickandmortyapi.com/api/character/2\",\n" +
            "\"https://rickandmortyapi.com/api/character/38\",\n" +
            "\"https://rickandmortyapi.com/api/character/46\",\n" +
            "\"https://rickandmortyapi.com/api/character/63\",\n" +
            "\"https://rickandmortyapi.com/api/character/80\",\n" +
            "\"https://rickandmortyapi.com/api/character/175\",\n" +
            "\"https://rickandmortyapi.com/api/character/221\",\n" +
            "\"https://rickandmortyapi.com/api/character/239\",\n" +
            "\"https://rickandmortyapi.com/api/character/246\",\n" +
            "\"https://rickandmortyapi.com/api/character/304\",\n" +
            "\"https://rickandmortyapi.com/api/character/305\",\n" +
            "\"https://rickandmortyapi.com/api/character/306\",\n" +
            "\"https://rickandmortyapi.com/api/character/329\",\n" +
            "\"https://rickandmortyapi.com/api/character/338\",\n" +
            "\"https://rickandmortyapi.com/api/character/396\",\n" +
            "\"https://rickandmortyapi.com/api/character/397\",\n" +
            "\"https://rickandmortyapi.com/api/character/398\",\n" +
            "\"https://rickandmortyapi.com/api/character/405\"\n" +
            "],\n" +
            "\"url\":\"https://rickandmortyapi.com/api/episode/2\",\n" +
            "\"created\":\"2017-11-10T12:56:33.916Z\"\n" +
            "},\n" +
            "{\n" +
            "\"id\":3,\n" +
            "\"name\":\"Anatomy Park\",\n" +
            "\"air_date\":\"December 16, 2013\",\n" +
            "\"episode\":\"S01E03\",\n" +
            "\"characters\":[\n" +
            "\"https://rickandmortyapi.com/api/character/1\",\n" +
            "\"https://rickandmortyapi.com/api/character/2\",\n" +
            "\"https://rickandmortyapi.com/api/character/12\",\n" +
            "\"https://rickandmortyapi.com/api/character/17\",\n" +
            "\"https://rickandmortyapi.com/api/character/38\",\n" +
            "\"https://rickandmortyapi.com/api/character/45\",\n" +
            "\"https://rickandmortyapi.com/api/character/96\",\n" +
            "\"https://rickandmortyapi.com/api/character/97\",\n" +
            "\"https://rickandmortyapi.com/api/character/98\",\n" +
            "\"https://rickandmortyapi.com/api/character/99\",\n" +
            "\"https://rickandmortyapi.com/api/character/100\",\n" +
            "\"https://rickandmortyapi.com/api/character/101\",\n" +
            "\"https://rickandmortyapi.com/api/character/108\",\n" +
            "\"https://rickandmortyapi.com/api/character/112\",\n" +
            "\"https://rickandmortyapi.com/api/character/114\",\n" +
            "\"https://rickandmortyapi.com/api/character/169\",\n" +
            "\"https://rickandmortyapi.com/api/character/175\",\n" +
            "\"https://rickandmortyapi.com/api/character/186\",\n" +
            "\"https://rickandmortyapi.com/api/character/201\",\n" +
            "\"https://rickandmortyapi.com/api/character/268\",\n" +
            "\"https://rickandmortyapi.com/api/character/300\",\n" +
            "\"https://rickandmortyapi.com/api/character/302\",\n" +
            "\"https://rickandmortyapi.com/api/character/338\",\n" +
            "\"https://rickandmortyapi.com/api/character/356\"\n" +
            "],\n" +
            "\"url\":\"https://rickandmortyapi.com/api/episode/3\",\n" +
            "\"created\":\"2017-11-10T12:56:34.022Z\"\n" +
            "}]"
    //endregion

    private val gson = Gson()

    @Test
    fun testSingleMappingToDomain() {

        val episodeEntity = gson.fromJson<EpisodeEntity>(
            singleEpisodeJson,
            EpisodeEntity::class.java
        )

        val episode = episodeEntity.mapToDomainModel()
        assertEntityToDomain(episodeEntity, episode)

    }

    @Test
    fun testListMappingToDomain() {

        val type = object : TypeToken<List<EpisodeEntity>>() {}.type
        val episodeEntities = gson.fromJson<List<EpisodeEntity>>(listOfEpisodesJson, type)

        val episodes = episodeEntities.mapToDomainModel()

        assert(episodes.size == episodeEntities.size)

        episodes.forEachIndexed { index, episode ->
            assertEntityToDomain(
                episodeEntities[index],
                episode
            )
        }

    }

    private fun assertEntityToDomain(entity: EpisodeEntity, episode: Episode) {
        val episodeNumber =
            if (episode.number < 10) "0${episode.number}" else episode.number.toString()
        val episodeSeason =
            if (episode.season < 10) "0${episode.season}" else episode.season.toString()
        val code = "S${episodeSeason}E$episodeNumber"

        assert(episode.id == entity.id)
        assert(episode.name == entity.name)
        assert(episode.airDate == entity.airDate)
        assert(code == entity.code)
        assert(episode.characterIds.size == entity.characterUrls.size)
    }

}