package com.mhr.rickmorty.data.datasource.remote

import com.mhr.rickmorty.domain.model.Character

interface CharacterRemoteDataSource {

    suspend fun getCharacters(page: Int = 1): List<Character>

    val canPaginate: Boolean

    suspend fun getCharactersByIds(ids: IntArray): List<Character>

}