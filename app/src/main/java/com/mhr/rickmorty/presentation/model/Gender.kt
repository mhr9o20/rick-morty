package com.mhr.rickmorty.presentation.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.mhr.rickmorty.R

enum class Gender(
    @DrawableRes val icon: Int,
    @StringRes val title: Int)
{
    Female(R.drawable.ic_gender_female_24dp, R.string.gender_female),
    Male(R.drawable.ic_gender_male_24dp, R.string.gender_male),
    Genderless(R.drawable.ic_genderless_24dp, R.string.gender_no),
    Unknown(R.drawable.ic_gender_unknown_24dp, R.string.gender_unknown)
}