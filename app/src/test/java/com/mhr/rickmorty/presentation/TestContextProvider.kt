package com.mhr.rickmorty.presentation

import com.mhr.rickmorty.di.CoroutineContextProvider
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

class TestContextProvider : CoroutineContextProvider() {
    override val Main: CoroutineContext = Dispatchers.Unconfined
    override val IO: CoroutineContext = Dispatchers.Unconfined
    override val Default: CoroutineContext = Dispatchers.Unconfined
}