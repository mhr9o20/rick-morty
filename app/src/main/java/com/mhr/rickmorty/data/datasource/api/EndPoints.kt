package com.mhr.rickmorty.data.datasource.api

object EndPoints {
    const val base = "https://rickandmortyapi.com/api/"
    const val episode = "episode/"
    const val character = "character/"
}