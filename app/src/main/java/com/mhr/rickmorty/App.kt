package com.mhr.rickmorty

import androidx.multidex.MultiDexApplication
import com.mhr.rickmorty.data.datasource.api.EndPoints
import com.mhr.rickmorty.di.AppComponent
import com.mhr.rickmorty.di.DaggerAppComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class App : MultiDexApplication() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        component = DaggerAppComponent
            .factory()
            .create(
                Retrofit.Builder()
                    .baseUrl(EndPoints.base)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            )
    }

}