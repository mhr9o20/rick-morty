package com.mhr.rickmorty.data.repository

import com.mhr.rickmorty.data.datasource.remote.EpisodeRemoteDataSource
import com.mhr.rickmorty.domain.model.Episode
import com.mhr.rickmorty.domain.repository.EpisodeRepository

class EpisodeRepositoryImpl(
    private val remoteDataSource: EpisodeRemoteDataSource
) : EpisodeRepository {

    override suspend fun getAllBy(page: Int): List<Episode> {
        return remoteDataSource.getEpisodes(page)
    }

    override val canPaginate: Boolean
        get() = remoteDataSource.canPaginate

}