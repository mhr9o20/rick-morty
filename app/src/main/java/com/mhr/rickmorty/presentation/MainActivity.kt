package com.mhr.rickmorty.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.mhr.rickmorty.R
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = findNavController(R.id.mainNavHostFragment)

        navController.addOnDestinationChangedListener {
                _,
                destination,
                _ ->

            supportActionBar?.title = destination.label

        }

        setupActionBarWithNavController(
            navController,
            AppBarConfiguration.Builder(navController.graph).build()
        )

    }

    override fun onSupportNavigateUp(): Boolean {
        return try {
            navController.navigate(R.id.action_popBack_to_charactersFragment)
            false
        } catch (e: Exception) {
            navController.navigateUp() || super.onSupportNavigateUp()
        }
    }

    override fun onBackPressed() {
        try {
            navController.navigate(R.id.action_popBack_to_charactersFragment)
        } catch (e: Exception) {
            super.onBackPressed()
        }
    }

}
