package com.mhr.rickmorty.domain.model

import java.util.*

/*
Entity:
    data class EpisodeEntity(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("air_date") val airDate: String,
    @SerializedName("episode") val code: String,
    @SerializedName("characters") val characterUrls: List<String>,
    @SerializedName("url") val ownPageUrl: String,
    @SerializedName("created") val creationDate: String
)
 */

data class Episode(
       val id: Int,
       val name: String,
       val airDate: String,
       val season: Int,
       val number: Int,
       val characterIds: List<Int>
)