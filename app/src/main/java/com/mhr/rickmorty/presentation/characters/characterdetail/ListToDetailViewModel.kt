package com.mhr.rickmorty.presentation.characters.characterdetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mhr.rickmorty.presentation.model.CharacterItem

class ListToDetailViewModel : ViewModel() {

    val selectedItem = MutableLiveData<CharacterItem>()
    var lastListPosition: Int = 0

}