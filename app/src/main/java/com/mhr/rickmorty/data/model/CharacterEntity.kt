package com.mhr.rickmorty.data.model

import com.google.gson.annotations.SerializedName
import com.mhr.rickmorty.domain.model.Character

/*
"id":361,
"name":"Toxic Rick",
"status":"Dead",
"species":"Humanoid",
"type":"Rick's toxic side",
"gender":"Male",
"origin":{
"name":"Detoxifier",
"url":"https://rickandmortyapi.com/api/location/64"
},
"location":{
"name":"Earth (Replacement Dimension)",
"url":"https://rickandmortyapi.com/api/location/20"
},
"image":"https://rickandmortyapi.com/api/character/avatar/361.jpeg",
"episode":[
"https://rickandmortyapi.com/api/episode/27"
],
"url":"https://rickandmortyapi.com/api/character/361",
"created":"2018-01-10T18:20:41.703Z"
 */
data class CharacterEntity(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("status") val status: String,
    @SerializedName("species") val species: String,
    @SerializedName("type") val type: String,
    @SerializedName("gender") val gender: String,
    @SerializedName("origin") val originLocation: LocationEntity,
    @SerializedName("location") val lastKnownLocation: LocationEntity,
    @SerializedName("image") val imageUrl: String,
    @SerializedName("episode") val episodeUrls: List<String>,
    @SerializedName("url") val ownPageUrl: String,
    @SerializedName("created") val creationDate: String
)

data class LocationEntity(
    @SerializedName("name") val name: String,
    @SerializedName("url") val url: String
)

fun CharacterEntity.mapToDomainModel(): Character {

    val firstAppearance: Int =
        if (episodeUrls.isEmpty()) Int.MAX_VALUE
        else {
            val split = episodeUrls[0].split("/")
            split[split.size - 1].toIntOrNull() ?: Int.MAX_VALUE
        }

    return Character(
        id = id,
        name = name,
        status = status,
        species = species,
        type = type,
        gender = gender,
        originLocationName = originLocation.name,
        lastKnownLocationName = lastKnownLocation.name,
        imageUrl = imageUrl,
        totalAppearance = episodeUrls.size,
        firstAppearance = firstAppearance
    )
}

fun List<CharacterEntity>.mapToDomainModel(): List<Character> = map { it.mapToDomainModel() }