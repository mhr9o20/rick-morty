package com.mhr.rickmorty.data.model

import com.google.gson.annotations.SerializedName
import com.mhr.rickmorty.domain.model.Episode

/*
"id": 1,
"name": "Pilot",
"air_date": "December 2, 2013",
"episode": "S01E01",
"characters": [
    "https://rickandmortyapi.com/api/character/1",
    "https://rickandmortyapi.com/api/character/2",
    //...
],
"url": "https://rickandmortyapi.com/api/episode/1",
"created": "2017-11-10T12:56:33.798Z"
 */
data class EpisodeEntity(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("air_date") val airDate: String,
    @SerializedName("episode") val code: String,
    @SerializedName("characters") val characterUrls: List<String>,
    @SerializedName("url") val ownPageUrl: String,
    @SerializedName("created") val creationDate: String
)

fun EpisodeEntity.mapToDomainModel(): Episode {

    val splitCode = code.split("e", ignoreCase = true)
    val seasonNumber = splitCode[0].replace("s", "", ignoreCase = true)
    val episodeNumber = splitCode[1]

    val characterIds: MutableList<Int> = mutableListOf()

    for (url in characterUrls) {
        val split = url.split("/")
        val id = split[split.size - 1]
        characterIds.add(id.toIntOrNull() ?: 0)
    }

    return Episode(
        id = id,
        name = name,
        airDate = airDate,
        season = seasonNumber.toIntOrNull() ?: 0,
        number = episodeNumber.toIntOrNull() ?: 0,
        characterIds = characterIds
    )

}

fun List<EpisodeEntity>.mapToDomainModel(): List<Episode> = map { it.mapToDomainModel() }