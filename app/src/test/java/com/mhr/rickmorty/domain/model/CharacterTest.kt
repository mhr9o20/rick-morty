package com.mhr.rickmorty.domain.model

import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_FEMALE
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_MALE
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_NO
import com.mhr.rickmorty.domain.model.Character.Companion.GENDER_UNKNOWN
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_ALIVE
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_DEAD
import com.mhr.rickmorty.domain.model.Character.Companion.STATUS_UNKNOWN
import com.mhr.rickmorty.presentation.model.CharacterItem
import com.mhr.rickmorty.presentation.model.Gender
import com.mhr.rickmorty.presentation.model.Status
import org.junit.Test

class CharacterTest {

    private val singleDomainModel = Character(
        id = 1,
        name = "Rick",
        status = STATUS_ALIVE,
        type = "type",
        species = "species",
        gender = GENDER_MALE,
        originLocationName = "Earth",
        lastKnownLocationName = "Unknown",
        imageUrl = "url",
        totalAppearance = 50,
        firstAppearance = 1
    )

    private val domainModelsList: List<Character> = listOf(
        singleDomainModel,
        Character(
            2, "n1", STATUS_DEAD, "s", "t", GENDER_NO, "o", "l", "i", 1, 2
        ),
        Character(
            3, "n2", STATUS_UNKNOWN, "s", "t", GENDER_FEMALE, "o", "l", "i", 3, 2
        ),
        Character(
            4, "n3", STATUS_UNKNOWN, "s", "t", GENDER_UNKNOWN, "o", "l", "o", 5, 3
        )

    )

    @Test
    fun testSingleMapping() {
        val characterItem = singleDomainModel.mapToPresentationModel()
        assertDomainToPresentation(singleDomainModel, characterItem)
    }

    @Test
    fun testListMapping() {
        val characterItems = domainModelsList.mapToPresentaionModel()
        characterItems.forEachIndexed { index, characterItem ->
            assertDomainToPresentation(
                domain = domainModelsList[index],
                presentation = characterItem
            )
        }
    }

    private fun assertDomainToPresentation(domain: Character, presentation: CharacterItem) {

        val status = when (domain.status) {
            STATUS_ALIVE -> Status.Alive
            STATUS_DEAD -> Status.Dead
            else -> Status.Unknown
        }

        val gender = when (domain.gender) {
            GENDER_FEMALE -> Gender.Female
            GENDER_MALE -> Gender.Male
            GENDER_NO -> Gender.Genderless
            else -> Gender.Unknown
        }

        assert(presentation.id == domain.id)
        assert(presentation.name == domain.name)
        assert(presentation.status == status)
        assert(presentation.type == domain.type)
        assert(presentation.species == domain.species)
        assert(presentation.gender == gender)
        assert(presentation.originLocationName == domain.originLocationName)
        assert(presentation.lastKnownLocationName == domain.lastKnownLocationName)
        assert(presentation.imageUrl == domain.imageUrl)
        assert(presentation.totalAppearance == domain.totalAppearance)
        assert(presentation.firstAppearance == domain.firstAppearance)

    }

}