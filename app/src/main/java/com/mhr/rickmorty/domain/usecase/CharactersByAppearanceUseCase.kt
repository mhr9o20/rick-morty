package com.mhr.rickmorty.domain.usecase

import com.mhr.rickmorty.domain.model.Character
import com.mhr.rickmorty.domain.repository.CharacterRepository
import com.mhr.rickmorty.domain.repository.EpisodeRepository

class CharactersByAppearanceUseCase(
    private val characterRepository: CharacterRepository,
    private val episodeRepository: EpisodeRepository
) {

    var chunkSize = 20
    private var canPaginateCharacters = true

    val canPaginate: Boolean
        get() = canPaginateCharacters

    private var currentEpisodesPage = 1

    private val characterIds: LinkedHashSet<Int> =
        linkedSetOf() //linkedSet to maintain insertion order

    suspend fun getCharactersOrderedByAppearanceBy(page: Int = 1): List<Character> {

        while (characterIds.size < page * chunkSize && episodeRepository.canPaginate) {
            fetchEpisodesBy(currentEpisodesPage)
        }

        val idsChunked = characterIds.chunked(chunkSize)

        if (page == idsChunked.size && episodeRepository.canPaginate.not()) {
            canPaginateCharacters = false
        }

        return characterRepository.getBy(idsChunked[page - 1].toIntArray())

    }

    private suspend fun fetchEpisodesBy(page: Int) {
        val episodes = episodeRepository.getAllBy(page)
        for (episode in episodes) {
            characterIds.addAll(episode.characterIds)
        }
        currentEpisodesPage++
    }

}