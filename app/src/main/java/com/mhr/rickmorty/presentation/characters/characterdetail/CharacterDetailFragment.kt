package com.mhr.rickmorty.presentation.characters.characterdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.mhr.rickmorty.R
import com.mhr.rickmorty.databinding.ViewCharacterDetailBinding

class CharacterDetailFragment : Fragment() {

    private lateinit var binding: ViewCharacterDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.view_character_detail,
            container,
            false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel: ListToDetailViewModel by activityViewModels()
        viewModel.selectedItem.observe(this,
            Observer {
                binding.character = it
            })
    }

}