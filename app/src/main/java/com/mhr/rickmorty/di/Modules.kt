package com.mhr.rickmorty.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mhr.rickmorty.data.datasource.api.CharacterApi
import com.mhr.rickmorty.data.datasource.api.EpisodeApi
import com.mhr.rickmorty.data.repository.CharacterRepositoryImpl
import com.mhr.rickmorty.data.repository.EpisodeRepositoryImpl
import com.mhr.rickmorty.datasource.api.CharacterRemoteDataSourceImpl
import com.mhr.rickmorty.datasource.api.EpisodeRemoteDataSourceImpl
import com.mhr.rickmorty.domain.repository.CharacterRepository
import com.mhr.rickmorty.domain.repository.EpisodeRepository
import com.mhr.rickmorty.domain.usecase.CharactersByAppearanceUseCase
import com.mhr.rickmorty.presentation.characters.CharactersViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Module(subcomponents = [ViewModelComponent::class])
class AppModule {

    @Provides
    @Singleton
    fun coroutineContextProvider() = CoroutineContextProvider()

    @Provides
    @Singleton
    fun episodeRepository(retrofit: Retrofit): EpisodeRepository {

        val remoteDataSource = EpisodeRemoteDataSourceImpl(
            retrofit.create(EpisodeApi::class.java)
        )

        return EpisodeRepositoryImpl(remoteDataSource)

    }

    @Provides
    @Singleton
    fun charactersRepository(retrofit: Retrofit): CharacterRepository {

        val remoteDataSource = CharacterRemoteDataSourceImpl(
            retrofit.create(CharacterApi::class.java)
        )

        return CharacterRepositoryImpl(remoteDataSource)

    }

    @Provides
    @Singleton
    fun charactersByAppearanceUseCase(
        episodeRepository: EpisodeRepository,
        characterRepository: CharacterRepository
    ) = CharactersByAppearanceUseCase(characterRepository, episodeRepository)

}

class ViewModelFactory @Inject constructor(private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        viewModels[modelClass]?.get() as T
}

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(CharactersViewModel::class)
    internal abstract fun charactersViewModel(viewModel: CharactersViewModel): ViewModel

}