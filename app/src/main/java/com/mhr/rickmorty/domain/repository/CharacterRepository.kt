package com.mhr.rickmorty.domain.repository

import com.mhr.rickmorty.domain.model.Character

interface CharacterRepository {

    suspend fun getAllBy(page: Int = 1): List<Character>
    suspend fun getBy(ids: IntArray): List<Character>
    val canPaginate: Boolean

}